using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Entry : MonoBehaviour
{
    void Awake()
    {
        if (GameManager.Instance.backtracking == false)
            GameManager.Instance.Forward();
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            GameManager.Instance.ChangeScene(SceneManager.GetActiveScene().buildIndex - 1);
            GameManager.Instance.backtracking = true;
        }
    }
}
