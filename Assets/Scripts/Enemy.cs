using UnityEngine;

public class Enemy : MonoBehaviour
{
    public float speed;
    public float groundCheckDistance;
    public LayerMask groundLayer;
    private bool movingRight = true;

    void Update()
    {
        // Usa raycasts para comprobar si hay suelo abajo y delante del enemigo.
        Vector2 checkPosition = transform.position + (movingRight ? Vector3.right : Vector3.left) * groundCheckDistance;
        RaycastHit2D groundInFront = Physics2D.Raycast(checkPosition, Vector2.down, groundCheckDistance, groundLayer);
        RaycastHit2D groundBeneath = Physics2D.Raycast(transform.position, Vector2.down, groundCheckDistance, groundLayer);

        if (!groundInFront.collider || groundBeneath.collider == null)
        {
            // Si no hay suelo delante o debajo cambia la direccion.
            Flip();
        }

        // Mueve al enemigo
        transform.Translate(Vector2.right * (movingRight ? 1 : -1) * speed * Time.deltaTime);
    }

    void Flip()
    {
        // Cambia la direccion del enemigo invirtiendo la escala.
        Vector3 newScale = transform.localScale;
        newScale.x *= -1;
        transform.localScale = newScale;
        movingRight = !movingRight;
    }
}