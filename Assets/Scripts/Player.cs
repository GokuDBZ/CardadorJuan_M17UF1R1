using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Player : MonoBehaviour
{
    private Rigidbody2D _rb;
    public float speed = 5f;
    public bool gravity = false; //false = gravedad normal, true = gravedad invertida
    private PlayerAnimation _playerAnim;
    private SpriteRenderer _sprite;
    public bool grounded;
    private float lastJump;
    public GameObject gameOver;
    public GameObject winscreen;

    // Start is called before the first frame update
    void Start()
    {
        _rb = GetComponent<Rigidbody2D>();
        _playerAnim = GetComponent<PlayerAnimation>();
        _sprite = GetComponent<SpriteRenderer>();

        grounded = false;
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape)) 
        {
            if (Time.timeScale == 1) GameManager.Instance.PauseGame();
            else GameManager.Instance.ResumeGame();
        }

        //Movimiento
        float horizontalInput = Input.GetAxisRaw("Horizontal");
        _rb.velocity = new Vector2(horizontalInput * speed, _rb.velocity.y);

        //Cambio de gravedad
        if (Input.GetKeyDown(KeyCode.Space) && grounded)
        {
            lastJump = Time.time;
            gravity = !gravity;
            grounded = false;
            SoundManager.instance.Play("Gravedad");
        }

        if (gravity == true)
        {
            _rb.gravityScale = -1f;
        }
        else if (gravity == false) { _rb.gravityScale = 1f; }

        _playerAnim.Move(horizontalInput);

        //Flip horizontal sprite
        if (horizontalInput > 0)
        {
            _sprite.flipX = false;
        }
        else if (horizontalInput < 0)
        {
            _sprite.flipX = true;
        }
        //Flip vertical sprite
        if (gravity == true)
        {
            _sprite.flipY = true;
        }
        else if (gravity == false)
        {
            _sprite.flipY = false;
        }
    }
    //Colisiones
    private void OnCollisionEnter2D(Collision2D collision)
    {
        //Check para solo poder saltar despues de pisar algun suelo
        if (collision.gameObject.layer == LayerMask.NameToLayer("Suelo") && Time.time - lastJump > 0.1f) grounded = true;
        //Muerte
        if (collision.gameObject.CompareTag("Peligro"))
        {
            SoundManager.instance.Play("Muerte");
            this.gameObject.SetActive(false);
            gameOver.SetActive(true);
        }
    }
    private void OnTriggerEnter2D (Collider2D collision)
    {
        //Muerte
        if (collision.gameObject.CompareTag("Peligro"))
        {
            SoundManager.instance.Play("Muerte");
            this.gameObject.SetActive(false);            
            gameOver.SetActive(true);            
        }
        if (collision.gameObject.name == "Final")
        {
            this.gameObject.SetActive(false);
            GameManager.Instance.pause.SetActive(true);
            winscreen.SetActive(true);
        }
    }
}
