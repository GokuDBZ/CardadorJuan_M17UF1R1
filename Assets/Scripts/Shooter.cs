using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shooter : MonoBehaviour
{
    
    public GameObject projectilePrefab;
    public Transform firePoint; // The point where the projectile will be spawned.
    public float cooldown;
    private float timePassed = 0f;

    void Update()
    {
        timePassed += Time.deltaTime;
        if (timePassed > cooldown)
        {
            Vector2 mousePosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            GameObject newProjectile = Instantiate(projectilePrefab, firePoint.position, Quaternion.identity);
            Projectile projectile = newProjectile.GetComponent<Projectile>();
            timePassed = 0f;
        }
    }
}
