using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Exit : MonoBehaviour
{
    void Awake()
    {
        if (GameManager.Instance.backtracking == true)
            GameManager.Instance.Backtracking();
        else { GameManager.Instance.Respawn(); }
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
            GameManager.Instance.backtracking = false;
        }
    }
}
