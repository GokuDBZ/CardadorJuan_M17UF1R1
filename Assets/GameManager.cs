using System;
using System.Collections;
using System.Collections.Generic;
using System.Numerics;
using System.Threading;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    public static GameManager Instance;
    public bool backtracking = false;
    public GameObject pause;
    void Awake()
    {
        if (Instance == null) 
        {
            Instance = this;
            DontDestroyOnLoad(this);
        }
        else if (Instance != this) Destroy(this.gameObject);

    }

    //Funcion para cambiar de escena
    public void ChangeScene(string scene) 
    {
        SceneManager.LoadScene(scene);
    }
    public void ChangeScene(int scene)
    {
        SceneManager.LoadScene(scene);
    }
    public void Forward()
    {
        GameObject player = GameObject.Find("Player");
        UnityEngine.Vector3 entrada = GameObject.Find("Entrada").transform.position;
        //player.GetComponent<SpriteRenderer>().flipX = false;
        entrada.x = entrada.x + 2;
        entrada.y = entrada.y - 1;
        player.transform.position = entrada;
        SoundManager.instance.Play("Escena");
    }
    //Funcion para colocar al personaje en el lugar correcto al volver hacia un nivel anterior
    public void Backtracking()
    {
        GameObject player = GameObject.Find("Player");
        UnityEngine.Vector3 salida = GameObject.Find("Salida").transform.position;
        //player.GetComponent<SpriteRenderer>().flipX = true;
        salida.x = salida.x - 2;
        salida.y = -7;
        player.transform.position = salida;
        SoundManager.instance.Play("Escena");
    }
    //Funciona para cerrar la aplicacion
    public void Cerrar()
    {
        Application.Quit();
    }
    public void Respawn()
    {
        GameObject player = GameObject.Find("Player");
        GameObject spawn = GameObject.Find("Spawn");
        player.transform.position = spawn.transform.position;
    }
    public void PauseGame()
    {
        Time.timeScale = 0;
        pause.SetActive(true);        
    }
    public void ResumeGame()
    {
        Time.timeScale = 1;
        pause.SetActive(false);
    }
}
