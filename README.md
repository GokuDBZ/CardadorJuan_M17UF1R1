# Instrucciones
- Movimiento = W/A/S/D o flechas
- Cambio de gravedad = Espacio
- Pausa = ESC
- Pulsar botones menus = Clic izquierdo

# Descripcion
- Monkey Jungle es un juego de plataformas donde asumes el papel de un mono. Este simio posee la extraordinaria habilidad de controlar la gravedad. Su misión es recuperar su preciado plátano, que por desgracia ha acabado dentro de un templo repleto de trampas.
- Tiene un estilo pixel art de 16x16 durante el gameplay
- Estilo pixel art o cartoon para los menus

